package randoop.generation;

import randoop.ExecutionOutcome;
import randoop.main.GenInputsAbstract;
import randoop.operation.MethodCall;
import randoop.operation.TypedClassOperation;
import randoop.operation.TypedOperation;
import randoop.reflection.TypeInstantiator;
import randoop.sequence.ExecutableSequence;
import randoop.sequence.Variable;
import randoop.types.ClassOrInterfaceType;
import randoop.types.JavaTypes;
import randoop.types.VoidType;

import java.util.List;
import java.util.Set;
import java.util.ArrayList;


/** Customised forward generator. */
/** Executes trampoline with generated input sequence **/
/** If sink method is reachable, terminates and prints input to console */

public class GadgetGenerator extends ForwardGenerator {



 //   private final TypeInstantiator instan;

    public GadgetGenerator(
            List<TypedOperation> operations,
            Set<TypedOperation> sideEffectFreeMethods,
            GenInputsAbstract.Limits limits,
            ComponentManager componentManager,
            RandoopListenerManager listenerManager,
            Set<ClassOrInterfaceType> classesUnderTest) {
        super(
                operations,
                sideEffectFreeMethods,
                limits,
                componentManager,
                /*stopper=*/ null,
                listenerManager,
                classesUnderTest);
    }

    @Override
    public List<ExecutableSequence> getErrorTestSequences() {
        return outSinkSeqs;
    }

    @Override
    public ExecutableSequence step() {
        return super.step();
    }
}
