#!/bin/bash
rm -rf out/*
mvn clean install dependency:copy-dependencies
java -cp target/instrumenter-1.0.0.jar:target/dependencies/* instrumenter.Transform --in input --out output --log log.txt
