package instrumenter.utils;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

public class JarUtils {

    private static Logger logger = LoggerFactory.getLogger(JarUtils.class);


    /**
     * List of JARs or exploded jars.
     *
     * @param directory parent directory.
     * @return list of dirs.
     * @throws IOException
     */
    public static List<File> listJars(File directory) throws IOException {
        Set<File> result = new HashSet<>();
        listJars(directory, null, 0, result);

        // sort by path leng to resolve sub-dirs first
        return new ArrayList<>(result).stream().sorted(
                (a, b) -> b.getAbsolutePath().length() - a.getAbsolutePath().length()
        ).collect(Collectors.toList());
    }




    /**
     * Unzip the input file to a directory.
     * @param file ZIP file
     * @param dest destination dir
     * @throws IOException error
     */
    public static void unzip(File file, File dest) throws IOException {
        try (ZipFile zipFile = new ZipFile(file)) {
            Enumeration<? extends ZipEntry> entries = zipFile.entries();
            while (entries.hasMoreElements()) {
                ZipEntry entry = entries.nextElement();
                File entryDestination = new File(dest, entry.getName());
                if (entry.isDirectory()) {
                    entryDestination.mkdirs();
                } else {
                    entryDestination.getParentFile().mkdirs();
                    try (InputStream in = zipFile.getInputStream(entry);
                         OutputStream out = new FileOutputStream(entryDestination)) {

                        IOUtils.copy(in, out);
                    }
                }
            }
        }
    }

    /**
     * Zip input dir in a zIP file
     * @param sourceDir input dir
     * @param outputFile dest zip file
     * @throws IOException error
     */
    public static void zip(File sourceDir, File outputFile) throws IOException {
        String root = sourceDir.getPath();
        if (!root.endsWith(File.separator)) {
            root = root + File.separator;
        }
        try (ZipOutputStream zipFile = new ZipOutputStream(new FileOutputStream(outputFile))) {
            zip(root, sourceDir.getPath(), zipFile);
        }
    }

    private static void zip(String rootDir, String sourceDir, ZipOutputStream out) throws IOException {
        for (File file : new File(sourceDir).listFiles()) {
            if (file.isDirectory()) {
                zip(rootDir, sourceDir + File.separator + file.getName(), out);
            } else {
                String entryName = file.getPath().replace(rootDir, "");
                String unixEntryName = entryName.replace(File.separatorChar, '/');
                ZipEntry entry = new ZipEntry(unixEntryName);
                out.putNextEntry(entry);

                try (FileInputStream in = new FileInputStream(sourceDir + File.separator + file.getName())) {
                    IOUtils.copy(in, out);
                }
            }
        }
    }


    private static void listJars(File file, File explodedParent, int level, Set<File> result)  {
        String name = file.getName();
        if (file.isDirectory()) {
            for (File child : file.listFiles()) {
                // we support exploded JARs, but only for one subdirectory. In deeper nesting, it is imposible
                // to detect what the root folder is.
                if (level == 0) {
                    explodedParent = child;
                }
                listJars(child, explodedParent, level + 1, result);
            }
        } else if (name.endsWith(".jar") || name.endsWith(".zip")) {
            result.add(file);
        } else if (file.isFile()) {
            // any file that is not JAR and ZIP may be exploded parent.
            result.add(explodedParent);
        }
    }




    private static boolean isSignature(String name) {
        return name.toUpperCase().endsWith(".SF") || name.toUpperCase().endsWith(".RSA");
    }

    private static boolean isJar(String name) {
        return name.endsWith(".jar");
    }

}
