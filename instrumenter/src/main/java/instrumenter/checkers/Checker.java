package instrumenter.checkers;

import java.io.*;
import java.util.Comparator;

public class Checker {

    public static boolean checking = true;
    public static void check(Object obj) {
        groovy.lang.GroovyObject g;
        checking = true;
        if (obj == null)
            return;
        try {
        obj.hashCode();
        if (obj instanceof Comparator) {
          ((Comparator) obj).compare(new Object(), new Object());
        }
        } catch (Exception e) {
            checking = false;
        }
        checking = false;
    }

    public static Object deserCheck(Object obj) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream out = null;
        ObjectInputStream in = null;
        Object o = null;
        byte[] bytes;
        try {
            out = new ObjectOutputStream(bos);
            out.writeObject(obj);
            out.flush();
            bytes = bos.toByteArray();
            bos.close();
            ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
            in = new ObjectInputStream(bis);
            o = in.readObject();
            bis.close();
        } catch (Exception e) {

        }
        return o;
    }
}
