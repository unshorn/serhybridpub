#!/bin/bash 
OUT_FOLDER=./out
for file in $(ls $OUT_FOLDER); do
  f=$(basename $file)
  if [ -d out/$file ]
  then
    TIME=$(tail -1 out/$file/log.txt | cut -d ' ' -f1)
    taintedsinks=0
    sinks=timeout
    if [ -f out/$file/ReachableSinks.csv ]
    then
    taintedsinks=$(wc -l out/$file/ReachableTaintedSinks.csv | cut -d ' ' -f 1)
    sinks=$(wc -l out/$file/ReachableSinks.csv | cut -d ' ' -f 1)
    paths=$(wc -l out/$file/hp.txt | cut -d ' ' -f 1)
    fi
    echo -e "$file,$TIME,$taintedsinks,$paths"
  fi
done
