#!/bin/bash
cd randoop
./gradlew build --exclude-task systemTest --exclude-task coveredTest --exclude-task test --exclude-task replacecallTest
cd ..
