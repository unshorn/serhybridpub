#!/usr/bin/env bash

cd datalog-souffle
cp -r ./custom $DOOP_HOME/souffle-logic/addons/
cp  ./open-programs/rules-ser.dl $DOOP_HOME/souffle-logic/addons/open-programs/
cp  ./open-programs/rules-all.dl $DOOP_HOME/souffle-logic/addons/open-programs/
cp  ./open-programs/rules-mgo.dl $DOOP_HOME/souffle-logic/addons/open-programs/
cp  ./open-programs/entry_points.dl $DOOP_HOME/souffle-logic/addons/open-programs/
cp  ./custom/dynamic-proxies.dl $DOOP_HOME/souffle-logic/main/reflection/
cp  ./reflection/* $DOOP_HOME/souffle-logic/main/reflection/

cd ..

