#!/usr/bin/env bash


PROJECT_HOME=$BASE_DIR
INPUT_HOME=$BASE_DIR/instrumenter/output
INPUT=$INPUT_HOME/$(basename $1 .jar)-instrumented.jar
ID=$(basename $1 .jar)
OUTPUT_DIR=$OUT_DIR/$(basename $1 .jar)
LOG_FILE=${OUTPUT_DIR}/log.txt
MACRO_FILE=./datalog-souffle/custom/macros.h
#DYN_PROXIES=--reflection-dynamic-proxies
PROJECT=$1
REFLECTION="--light-reflection-glue"
#INFO_FLOW="--information-flow minimal"
#JIMPLE=" --generate-jimple"
CACHE=--cache
if [ -d $OUTPUT_DIR ]
then
    true
else
    mkdir $OUTPUT_DIR
fi

rm $MACRO_FILE
touch $MACRO_FILE
touch $LOG_FILE
ANALYSIS=context-insensitive
TEST=YES

if [ $TEST == YES ]; then
  echo "#define TEST 1" | tee -a $MACRO_FILE
fi

./scripts/sync.sh
cp $BASE_DIR/instrumenter/log.log ~/s.csv
function save_start_time() {
 start=$(date +%s)
}

function save_end_time() {
 end=$(date +%s)
}

function log_duration() {
 echo $((end-start)) " seconds " $1 | tee -a $LOG_FILE
}

cd $DOOP_HOME

save_start_time


## Additional libs not required as we would like all classes in classpath in. option for libs:  -l ${ADDITIONAL_LIBS}
eval "${DOOP_HOME}/doop -i ${INPUT}  ${EXTRA_ARG} -a $ANALYSIS -id ${ID}  $DYN_PROXIES  $INFO_FLOW  $REFLECTION --Xstats-none $CACHE --souffle-jobs 4 --open-programs all $JIMPLE"


echo "Exit code $?" | tee -a $LOG_FILE 

save_end_time

log_duration "points-to"

cd -
files="CallGraphEdge.csv ReachableSinks.csv ReachableTaintedSinks.csv SinkVariable.csv FlowPath.csv AIPT.csv IPT.csv FlowPathVarTypes.csv"

for file in $files; do
  eval "cp $DOOP_HOME/out/$ANALYSIS/$ID/database/$file $OUTPUT_DIR/"
done

#rm -rf $DOOP_HOME/out/*
#rm -rf $DOOP_HOME/cache/*



