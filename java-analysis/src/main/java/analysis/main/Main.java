package analysis.main;

import analysis.graph.Path;
import analysis.heap.HeapModel;
import analysis.heap.InstanceNode;
import analysis.utils.Importer;
import analysis.utils.NeoUtils;
import org.apache.commons.cli.*;
import org.apache.commons.io.output.NullOutputStream;


import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashSet;

public class Main {


    public static void main(String[] args) throws Exception {

        Options options = new Options();

        options.addOption(Option.builder().longOpt("out").argName("file").desc("Output directory with doop files").hasArg().build());
        options.addOption(Option.builder().longOpt("log").argName("log").desc("Name of the log file").hasArg().build());
        options.addOption(Option.builder().longOpt("t").argName("type").desc("flowinstance|heapinstance|test").hasArg().build());

        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = parser.parse(options, args);

        if (!(cmd.hasOption("out") && cmd.hasOption("t"))) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("paths", options);
            return;
        }

        OutputStream logStream = new NullOutputStream();

        File out = new File(cmd.getOptionValue("out"));

        if (cmd.hasOption("log")) {
            File log = new File(cmd.getOptionValue("log"));
            logStream = new FileOutputStream(log);
        }
        NeoUtils.importDoopGraph(out);
        ArrayList<String> strings = NeoUtils.allDoopSimplePaths(out);

        if (cmd.getOptionValue("t").equals("flowinstance")) {
            NeoUtils.pathsToFile (out.getAbsolutePath()+"/paths.txt", strings);
        } else if (cmd.getOptionValue("t").equals("heapinstance")) {
            HeapModel hm = Importer.createHeapModel(out);
            HashSet<String> haps = new HashSet<>();
            for(String s: strings) {
                Path p = new Path(s, "\\|", true);
                try {
                    InstanceNode instance = hm.instantiate(hm.transform(p));
                    haps.add(instance.hap);
                } catch (Exception e) {
                    System.err.println("Error!");
                }
            }
            NeoUtils.pathsToFile(out.getAbsolutePath() + "/hp.txt", new ArrayList<>(haps));

        } else if (cmd.getOptionValue("t").equals("test")) {
            HeapModel hm = Importer.createHeapModel(out);
            for(String s: strings) {
                Path p = new Path(s, "\\|", true);
                Path e = hm.transform(p);
                InstanceNode instance = hm.instantiate(e);
                Object o = instance.instantiate();
                instrumenter.checkers.Checker.check(o);
            }

        }

    }

}