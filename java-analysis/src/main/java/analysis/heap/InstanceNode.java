package analysis.heap;

import analysis.reflection.ReflectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.*;

public class InstanceNode implements Serializable {

    private static Logger logger = LoggerFactory.getLogger(InstanceNode.class);

    private String original_alloc;
    private String allocation;
    private String type;
    private ArrayList<InstanceNode> array;
    public HashMap<String, InstanceNode> fields;
    private Object object;
    private int valueIndex;
    public String hap;

    public boolean isArray() {
        if (type.contains("["))
            return true;
        else
            return false;
    }
    public boolean isPrimitive() {
        switch(type) {
            case "int":
            case "char":
            case "float":
            case "byte":
            case "boolean":
            case "double":
                return true;
            default:
                return false;
        }
    }

    public String getType() {
        return type;
    }

    public String getAllocation() {
        return original_alloc;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InstanceNode that = (InstanceNode) o;
        return Objects.equals(original_alloc, that.original_alloc) &&
                Objects.equals(allocation, that.allocation) &&
                Objects.equals(type, that.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(allocation, type);
    }

    public InstanceNode(String allocation, String type) {
        Objects.requireNonNull(allocation, "Allocation must have an identifier");
        Objects.requireNonNull(type, "Allocation must have a type");

        fields = new HashMap<String, InstanceNode>();
        array = new ArrayList<InstanceNode>();

        this.allocation = allocation;
        original_alloc = allocation;
        this.type = type;
    }


    public InstanceNode copy() {
        InstanceNode in = new InstanceNode(this.allocation, this.type);
        return in;
    }

    public void storeField(String signature, InstanceNode instance) {
        fields.put(signature, instance);
    }

    public void storeIndex(String index, InstanceNode instance) {
        array.add(instance);
    }

    public String getComponentType() {
        if (isArray()) {
            return type.substring(0, type.length()-2);
        }
        return "";
    }

    public String toString() {
        String output = "";
        List<InstanceNode> list = new ArrayList<InstanceNode>();
        list.add(this);
        while(!list.isEmpty()) {
            InstanceNode i = list.get(0);
            list.remove(i);
            String objectType = "null";
            if (i.getObject() != null) {
                if (i.getObject() instanceof String)
                    objectType = (String) i.getObject();
                else
                    objectType = i.getObject().getClass().toString();
            }
            output = output + i.type+" "+(i.getObject()==null?"null":objectType)+"\n";
            for(String key: i.fields.keySet()) {
                output = output + "var"+i.allocation+"."+key+" = "+i.fields.get(key).allocation+"\n";
                list.add(i.fields.get(key));
            }
            for(InstanceNode a: i.array) {
                output = output + "var"+i.allocation+"[] = " + a +"\n";
                list.add(a);
            }
        }
        return output;
    }


    public void setObject()  {

        if (isPrimitive())
            return;

        logger.debug("Creating: "+type);

        Object o;
        if (type.equals("null_type")) {
            o = null;
            return;
        }

        if (type.equals("java.lang.Class")) {
            object =  HeapModel.getInstance().getClassConstant(valueIndex);
            return;
        }

        if (type.equals("java.lang.String")) {
            object =  HeapModel.getInstance().getStringConstant(valueIndex);
            return;
        }

        if (!isArray())
            try {
                o = ReflectUtils.instantiate(Class.forName(getType()));
            } catch (Exception e) {
                logger.error("Error creating error type: "+this.type+" "+getComponentType());
                o = null;
            }
        else
        {
            int size = this.array.size();
            String componentType = getComponentType();
            if (componentType.equals("int"))
                o = Array.newInstance(Integer.TYPE, size);
            else if (componentType.equals("byte"))
                o = Array.newInstance(Byte.TYPE, size);
            else {
                try {
                    if (getComponentType().contains("[")) {
                        String c = getComponentType();
                        c = c.replace("[", "");
                        c = c.replace("]","");
                        Object n;
                        if (c.equals("int"))
                            n = Array.newInstance(Integer.TYPE,1);
                        if (c.equals("byte"))
                            n = Array.newInstance(Byte.TYPE,1);
                        else
                            n = Array.newInstance(Class.forName(c),1);

                        o = Array.newInstance(n.getClass(), size);

                    } else
                    o = Array.newInstance(Class.forName(getComponentType()), size);
                } catch (Exception e) {
                    e.printStackTrace();
                    logger.error("Error creating error type: "+this.type+" "+getComponentType());
                    o = null;
                }
            }
        }

        object = o;
    }

    public Object getObject() {
        if (object == null)
            try {
                setObject();
            } catch (Exception e) {
                e.printStackTrace();
                System.exit(-1);
            }
         return object;
    }

    public Object instantiate() {

        FieldSignature field = null;

        try {
            ArrayList<InstanceNode> list = new ArrayList<InstanceNode>();
            list.add(this);
            while (!list.isEmpty()) {
                InstanceNode i = list.get(0);
                list.remove(i);
                if (i.getObject() == null) {
                    i.setObject();
                }
                for (String key : i.fields.keySet()) {
                    field = new FieldSignature(key);
                    InstanceNode value = i.fields.get(key);
                    if (value.getObject() == null)
                        value.setObject();
                    try {
                        ReflectUtils.setFieldValue(i.getObject(), value.getObject(), field.getName());
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                        System.exit(1);
                    }
                    list.add(i.fields.get(key));
                }
                for (InstanceNode a : i.array) {
                    InstanceNode component = a;
                    if (component.getObject() == null) {
                        component.setObject();
                    }
                    try {
                        Array.set(i.getObject(), 0, component.getObject());
                    } catch (Exception e) {

                    }
                    list.add(a);
                }
            }
        } catch (Exception e) {
            logger.error("Error instantiating: "+e.getMessage()+getType()+" "+field+" ");
        }
        return this.object;
    }
}
