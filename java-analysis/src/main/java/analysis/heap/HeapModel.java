package analysis.heap;

import analysis.graph.Edge;
import analysis.graph.Path;
import analysis.graph.Vertex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.util.*;

public class HeapModel {

    private static Logger logger = LoggerFactory.getLogger(HeapModel.class);
    private Map<String, Map<String, List<InstanceNode>>> fldPtsTo;
    private Map<String, List<InstanceNode>> arrayIndexPtsTo;

    // all variables in relevant analysis.heap paths that have field/array loads
    private  Map<String, VariableNode> variables ;

    private List<String> strings;
    private List<Class> classes;

    private static HeapModel hm;

    public HeapModel(Map<String, Map<String, List<InstanceNode>>> fldPtsTo,
                     Map<String, List<InstanceNode>> arrayIndexPtsTo,
                     Map<String, VariableNode> variables,
                     List<String> strings,
                     List<Class> classes
                     ) {
        this.fldPtsTo = fldPtsTo;
        this.arrayIndexPtsTo = arrayIndexPtsTo;
        this.variables = variables;
        this.strings = strings;
        this.classes = classes;
        hm = this;

    }

    public static HeapModel getInstance() {
        return hm;
    }

    public String getStringConstant(int r) {
        return strings.get(r % strings.size());

    }

    public Class getClassConstant(int r) {
        return classes.get(r % classes.size());

    }

    public InstanceNode instantiate(Path path) {

        String hap = "";
        InstanceNode root = null;
        InstanceNode current = null;
        List<Edge> edges = path.getEdges();

        for(int i = 0; i < edges.size(); i++) {
            Edge edge = edges.get(i);
            Vertex start = edge.getStart();
            Vertex end = edge.getEnd();
            String label = edge.getLabel();
            if (root == null) {
                if (start instanceof VariableNode) {
                    VariableNode v = (VariableNode) start;
                    root = v.sample();
                    hap += root.getType();
                } else {
                    root = new InstanceNode("-1", "java.lang.String");
                    hap += "str";

                }
                current = root;
            }
            if (label.equals("assign") || label.equals("bridge")) {
                continue;
            } else if (label.equals("arrayLoad")) {
                VariableNode v = (VariableNode) end;
                InstanceNode j = v.sample();
                j.storeIndex("0", current);
                current = j;
                hap += ".[]";

            } else {
                VariableNode v = (VariableNode) end;
                InstanceNode j = v.sample();
                j.storeField(label, current);
                current = j;
                hap += "."+label;
            }

        }
        current.hap = hap;
        return current;
    }

    // add points-to information for variable nodes in path
    public Path transform(Path path) {
        List<Edge> edges = path.getEdges();
        Path newPath = new Path();

        for(Edge e:edges) {
            String startId = e.getStart().getId();
            String endId = e.getEnd().getId();
            Vertex start = variables.get(startId);
            Vertex end = variables.get(endId);
            assert start != null;
            assert end != null;

            Edge newEdge = new Edge(e.getStart(), e.getEnd(), e.getLabel());

            if (start != null) {
                newEdge.setStart(start);
            }
            if (end != null) {
                newEdge.setEnd(end);
            }
            newPath.addEdge(newEdge);
        }
        return newPath;
    }
}
