package analysis.heap;

import java.util.Objects;

public class FieldSignature {

    private String type;
    private String classType;
    private String name;
    private String sig;

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public FieldSignature(String sig) {
            this.sig = sig;
            sig = sig.trim();
            sig = sig.substring(1, sig.length() - 1);
            sig = sig.replaceAll("\\:", "");
            String[] components  = sig.split(" ");
            this.type = components[1];
            this.name = components[2];
            this.classType = components[0].trim();
    }

    public int hashCode() {
        return Objects.hash(classType, type, name);
    }

    public boolean equals(Object o) {
        if (!(o instanceof FieldSignature))
            return false;
        FieldSignature other = (FieldSignature) o;
        return (other.getType().equals(this.getType()) && other.getName().equals(this.getName()));

    }

    public String toString() {
        return "<" + classType + ": " + type+" "+name + ">";
    }
}

