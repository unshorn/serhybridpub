package analysis.utils;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang.StringUtils;
import org.neo4j.graphalgo.GraphAlgoFactory;
import org.neo4j.graphalgo.PathFinder;
import org.neo4j.graphdb.*;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class NeoUtils {

    public static GraphDatabaseService graphDb;
    public static HashMap<String,Node> nodes = new HashMap<String,Node>();


    private static Logger logger = LoggerFactory.getLogger(NeoUtils.class);

    public static void importDoopGraph(File outDir) throws Exception {

        String outDirName = outDir.getAbsolutePath();
        File file = new File(outDirName + "/doop.neo4j");
        file.delete();
        GraphDatabaseFactory graphDbFactory = new GraphDatabaseFactory();
        graphDb = graphDbFactory.newEmbeddedDatabase(
                new File(outDirName + "/doop.neo4j"));



        Reader in = new FileReader(outDirName +"/FlowPath.csv");
        Iterable<CSVRecord> records =   CSVFormat.EXCEL
                        .withIgnoreEmptyLines()
                        .withRecordSeparator('\n')
                        .withDelimiter('\t').withTrim().parse(in);


        graphDb.beginTx();

        for (CSVRecord record : records) {

            String from = record.get(1).replaceAll("\"","").trim();
            String to = record.get(2).replaceAll("\"","").trim();
            String label = record.get(0).replaceAll("\"","").trim();


                Node fromNode = nodes.get(from);
                Node toNode = nodes.get(to);

                if (fromNode == null) {
                    fromNode = graphDb.createNode();
                    fromNode.setProperty("name", from);
                    nodes.put(from, fromNode);

                }
                if (toNode == null) {
                    toNode = graphDb.createNode();
                    toNode.setProperty("name", to);
                    nodes.put(to, toNode);
                }
                fromNode.createRelationshipTo(toNode, newRelType(label));

            }

        logger.info("Completed import!");
    }

    public static RelationshipType newRelType(String label) {
        // removed for 1.7 compatibility. need to operate at 1.7 as doop analysis is on 1.7
        return RelationshipType.withName(label);

    }


    public static void pathsToFile(String fileName, ArrayList<String> paths) throws IOException {
        File file = new File(fileName);
        FileWriter fw = new FileWriter(file);
        for (String s : paths) {
            fw.write(s + "\n");
        }
        fw.close();
    }


    public static ArrayList<String> allDoopSimplePaths(File outDir) {

        String outDirName = outDir.getAbsolutePath()  + "/";

        String sourceVariablesFile = outDirName + "SourceVariable.csv";
        String sinkVariableFile = outDirName + "SinkVariable.csv";
        String sourceVariable = "";
        String line = "";

        ArrayList<String> sinks = new ArrayList<String>();

        try (BufferedReader br = new BufferedReader(new FileReader(sinkVariableFile))) {

            while ((line = br.readLine()) != null) {
                sinks.add(line.trim());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try (BufferedReader br = new BufferedReader(new FileReader(sourceVariablesFile))) {

            sourceVariable = br.readLine().trim();

        } catch (Exception e) {
            e.printStackTrace();
        }


        ArrayList<String> paths = new ArrayList<String>();

        PathExpanderBuilder peb = PathExpanderBuilder.allTypes(Direction.OUTGOING);

        PathExpander pe = PathExpanders.forDirection(Direction.OUTGOING);


        PathExpander<Path> build = peb.build();

        //PathFinder<Path> pf = GraphAlgoFactory.allPaths(build, 10);
        PathFinder<Path> pf = null;

        Node sourceNode = nodes.get(sourceVariable);

        for(String s: sinks) {
            Node sinkNode = nodes.get(s);
            java.lang.reflect.Constructor c;
            int d = 1;
            Iterable<Path> i = null;
            while(d < 50) {
                pf = GraphAlgoFactory.shortestPath(build, d);
                i = pf.findAllPaths(sourceNode, sinkNode);
                if (i.iterator().hasNext())
                    break;
                d++;
            }
            for (Path p : i) {
                ArrayList<String> sb = new ArrayList<String>();
                for (PropertyContainer pc : p) {
                    if (pc instanceof Node) sb.add(((Node)pc).getProperty("name").toString());
                    else sb.add(((Relationship)pc).getType().name());

                }
                sb.remove(0);
                sb.remove(0);
                sb.remove(0);
                paths.add(StringUtils.join(sb.toArray(new String[]{}),"|"));
            }
        }

        return paths;

    }

}
