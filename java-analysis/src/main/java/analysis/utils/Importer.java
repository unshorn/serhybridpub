package analysis.utils;

import analysis.heap.HeapModel;
import analysis.heap.InstanceNode;
import analysis.heap.VariableNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Importer {

    private static  String VAR_TYPES_FILE;
    private static  String INSTANCE_FLD_PTS_FILE;
    private static  String ARRAY_INDEX_PTS_FILE;
    private static String CLASSES_CONSTANTS_FILE;
    private static String STRINGS_CONSTANTS_FILE;

    private static  Map<String, Map<String, List<InstanceNode>>> fldPtsTo;
    private static Map<String, List<InstanceNode>> arrayIndexPtsTo;
    private static List classes;
    private static List strings;
    private static HashMap<String, VariableNode> variables ;

    private static Logger logger = LoggerFactory.getLogger(Importer.class);


    public static HeapModel createHeapModel(File outDir) {
        String outDirName = outDir.getAbsolutePath() + "/" ;
        VAR_TYPES_FILE = outDirName + "FlowPathVarTypes.csv";
        INSTANCE_FLD_PTS_FILE = outDirName + "IPT.csv";
        ARRAY_INDEX_PTS_FILE = outDirName + "AIPT.csv";
        CLASSES_CONSTANTS_FILE = outDirName + "/classes.txt";
        STRINGS_CONSTANTS_FILE = outDirName  + "/strings.txt";
        loadArrayIndexPtsTo();
        loadInstanceFldPtsTo();
        loadVarTypes();
        loadConstants();
        HeapModel hm = new HeapModel(fldPtsTo, arrayIndexPtsTo, variables, strings, classes);
        return hm;
    }


    private static void loadArrayIndexPtsTo() {
        arrayIndexPtsTo = new HashMap<String, List<InstanceNode>>();
        try (BufferedReader br = new BufferedReader(new FileReader(ARRAY_INDEX_PTS_FILE));) {
            String line;
            while (( line = br.readLine()) != null) {
                line = line.trim();
                // base, value, type
                String[] components = line.split("\t");
                components[0] = components[0].trim();
                components[1] = components[1].trim();
                components[2] = components[2].trim();

                List<InstanceNode> list = arrayIndexPtsTo.get(components[0]);

                if (list == null) {
                    list = new ArrayList<InstanceNode>();
                    arrayIndexPtsTo.put(components[0], list);
                }
                // alloc, type
                list.add(new InstanceNode(components[1], components[2]));



            }
        } catch (IOException e) {
            logger.error("Error reading: "+ARRAY_INDEX_PTS_FILE);
            System.exit(-1);
        }

        logger.info("ArrayIndexPointsTo size: "+arrayIndexPtsTo.size());
    }


    private static void loadInstanceFldPtsTo() {
        fldPtsTo = new HashMap<String,Map<String,List<InstanceNode>>>();
        List<InstanceNode> instances = new ArrayList<>();
        Map<InstanceNode, InstanceNode> instancesSet = new HashMap<>();

        try (BufferedReader br = new BufferedReader(new FileReader(INSTANCE_FLD_PTS_FILE));) {
            String line;
            while (( line = br.readLine()) != null) {
                line = line.trim();
                // base, sig, value, type
                String[] components = line.split("\t");
                components[0] = components[0].trim();
                components[1] = components[1].trim();
                components[2] = components[2].trim();
                components[3] = components[3].trim();


                Map<String,List<InstanceNode>> p = fldPtsTo.get(components[0]);
                if (p == null) {
                    p = new HashMap<String,List<InstanceNode>>();
                    fldPtsTo.put(components[0], p);
                }
                List<InstanceNode> list = p.get(components[1]);
                if (list == null) {
                    list = new ArrayList<InstanceNode>();
                    p.put(components[1], list);
                }
                InstanceNode instanceNode = new InstanceNode(components[2], components[3]);
                InstanceNode item = instancesSet.get(instanceNode);
                if (item != null) {
                    instanceNode = item;
                    // instances.put(components[1], instanceNode);
                } else {
                    instancesSet.put(instanceNode, instanceNode);
                }
                list.add(instanceNode);


            }

        } catch (IOException e) {
            logger.error("Error reading: "+INSTANCE_FLD_PTS_FILE);
            System.exit(-1);
        }
        logger.info("InstanceFieldPointsTo size: "+fldPtsTo.size());

    }

    public static void loadConstants()  {
        classes = new ArrayList<Class>();
        strings = new ArrayList<String>();

        try (BufferedReader br = new BufferedReader(new FileReader(CLASSES_CONSTANTS_FILE));) {
            String line;
            while ((line = br.readLine()) != null) {
                Class klass = Class.forName(line);
                classes.add(klass);

            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            logger.error("Error loading constants");
            System.exit(-1);
        }

        try (BufferedReader br = new BufferedReader(new FileReader(STRINGS_CONSTANTS_FILE));) {
            String line;
            while ((line = br.readLine()) != null) {
                strings.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
            logger.error("Error loading constants");
            System.exit(-1);

        }
    }

    private  static void loadVarTypes() {

        variables = new HashMap<String, VariableNode>();
        Map<InstanceNode, InstanceNode> instancesSet = new HashMap<>();
        try (BufferedReader br = new BufferedReader(new FileReader(VAR_TYPES_FILE));) {
            String line;
            while (( line = br.readLine()) != null) {
                line = line.trim();
                // type, val, var
                String[] components = line.split("\t");
                components[0] = components[0].trim();
                components[1] = components[1].trim();
                components[2] = components[2].trim();


                InstanceNode instanceNode = new InstanceNode(components[1], components[0]);
                InstanceNode item = instancesSet.get(instanceNode);
                if (item != null) {
                    instanceNode = item;
                    // instances.put(components[1], instanceNode);
                } else {
                    instancesSet.put(instanceNode, instanceNode);
                }

                VariableNode v = variables.get(components[2]);
                if (v == null) {
                    v = new VariableNode(components[2]);
                    variables.put(components[2], v);
                }

                v.add(instanceNode);
            }
        } catch (IOException e) {
            logger.error("Error reading: "+VAR_TYPES_FILE);
            System.exit(-1);
        }
        logger.info("Vars size: "+variables.size());

    }

}
