(defproject javaanalysis "1.0.0-SNAPSHOT"
  :description "Security Analysis for Java Serialisation"
  :dependencies [[org.clojure/clojure "1.8.0"]

                 [compojure "1.5.2"]
                 [org.neo4j/neo4j "3.2.7"]
                 [org.clojure/data.json "0.2.6"]
                 [org.ow2.asm/asm "5.2"]
                 [org.clojure/tools.logging "0.4.0"]

                 [org.clojure/tools.cli "0.3.7"]


                 ; program analysis
                 
                 [soot/soot "2.5.0"]
                 [com.ibm.wala/com.ibm.wala.core "1.4.3"]
                 [org.javassist/javassist "3.19.0-GA"]                 

                 ; library to analyse
                 [commons-collections/commons-collections "3.2.1"]
               ; [local/acc "3.2.1"] ; instrumented commons collections 3.2.1

                 [org.apache.commons/commons-collections4 "4.0"]

                 [clj-http "2.3.0"]
                 [ring/ring-defaults "0.2.1"]
                 [ring/ring-json "0.4.0"]

                 ; dependencies for gquery
                 [com.google.guava/guava "13.0.1"]
                 [org.antlr/antlr-runtime "3.2"]
                 [org.mvel/mvel2 "2.1.3.Final"]
                 [org.jdom/jdom "1.1"]
                 [junit/junit "4.10"]
                 
                ; [local/gql4jung "1.0.0"]

                 [net.sf.jung/jung-api "2.0"]
                 [net.sf.jung/jung-algorithms "2.0"]
                 [net.sf.jung/jung-io "2.0"]
                 [net.sf.jung/jung-graph-impl "2.0"]

                 [org.apache.commons/commons-csv "1.5"]
                 [clojure-csv/clojure-csv "2.0.1"]


                  ; dependencies for randoop
                 [com.github.javaparser/javaparser-core "2.4.0"]
                 [com.google.code.gson/gson "2.8.0"]
                 [junit/junit "4.10"]
                 [org.hamcrest/hamcrest-all "1.3"]
                 
                 [commons-lang/commons-lang "2.3"]
                 [junit/junit "3.8.1"]
                 [gnu.getopt/java-getopt "1.0.13"]
                 
;                [gnu-getopt/getopt "1.0.10"]
                 
                 [log4j/log4j "1.2.17" :exclusions [javax.mail/mail
                                                   javax.jms/jms
                                                  com.sun.jmdk/jmxtools
                                                 com.sun.jmx/jmxri]]

                 ; vulnerabilities
                 ; BeanShell
                 [org.beanshell/bsh "2.0b5"]

                 ; C3P0
                 [com.mchange/c3p0 "0.9.5.2"]
                 [com.mchange/mchange-commons-java "0.2.11"]

                 ; common-beanutils
                 [commons-beanutils/commons-beanutils "1.9.2"]
                 [commons-logging/commons-logging "1.2"]

                 ;groovy

                 [org.codehaus.groovy/groovy "2.3.9"]

                  ; jython
                 [org.python/jython-standalone "2.5.2"]

                 ; rhino
                 [rhino/js "1.7R2"]

                 ;rome
                 [rome/rome "1.0"]

                 ; hibernate
                 [org.hibernate/hibernate-core "5.0.7.Final"]

; weld
                 [org.jboss.weld/weld-core "1.1.33.Final"]


                 ]
  :plugins [[lein-ring "0.12.1"] [lein-localrepo "0.5.4"]]
  :ring {:handler fazoo.web.core/app}
  :source-paths      ["src/clojure"]
  :javac-options     ["-target" "1.7" "-source" "1.7"]
  :java-source-paths ["src/java"  "src/guery/core" "src/guery/jung" "src/guery/tests" "src/randoop-agent"]
  :repositories {"maven"   {:url "https://conjars.org/repo/"}}
  :main fazoo.main.core
  :jvm-opts ["-XX:MaxDirectMemorySize=7864m"] 
  
  )

